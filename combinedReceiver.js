const http = require('http')
const fs = require('fs')

function sendFileContent(res, fileName, contentType) {
    fs.readFile(fileName, 'utf8', (err, data) => {
        if (err) {
            res.writeHead(404);
            res.write('Not found');
        } else {
            res.writeHead(200, {'contentType': contentType});
            res.write(data);
        }
        return res.end();
    });
}

function processGetRequest(req, res) {
    {
        console.log("GET");
        console.log(req.url.toString());
        if (req.url.toString() === '/') {
            console.log("sending index html");
            sendFileContent(res, "index.html", "text/html");
        }

        if (/^\/[a-zA-Z0-9\/]*.css$/.test(req.url.toString())) {
            console.log('sending CSS');
            sendFileContent(res, req.url.toString().substring(1), "text/css");
        }
        if (/^\/[a-zA-Z0-9\/]*.ico$/.test(req.url.toString())) {
            console.log('sending ICO');
            sendFileContent(res, "/favicon.ico", "file");
        }
    }
}

function processPostRequest(req, res) {
    {
        let body = ''
        console.log('POST')
        req.on('data', function (data) {
            body += data

        })
        req.on('end', function () {
            console.log(body)
            let parsed = JSON.parse(body)
            let a = parseFloat(parsed['num1'])
            let b = parseFloat(parsed['num2'])
            let c = a + b;
            console.log(c)
            res.write(c.toString())
            res.end();
        })

    }
}

function getRequestListener() {
    return function (req, res) {
        // res.writeHead(200, {'Content-Type': 'text/html'})


        if (req.method === 'GET') processGetRequest(req, res);

        if (req.method === 'POST') processPostRequest(req, res);
    };
}

http.createServer(getRequestListener()).listen(8070);
